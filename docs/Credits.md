# Credits

Playing with the Sun was conceived by Amos Blanton and Ben Mardell, and is part of Amos' PhD work supported by Aarhus Public Libraries and the [Experimenting, Experiencing, Reflecting project](https://www.eer.info/). The construction kit is co-developed by [Amos Blanton](https://www.amosamos.net) and [Mark Moore](https://moore.dk/). Activity design is in collaboration with the project Creative Tech Literacy at Aarhus Libraries with funding from the the Danish Agency for Culture and Palaces. 

The team at Aarhus Public Libraries includes: Sidsel Bech-Petersen, Jane Kunze, Henrik Viking Hansen, Mathias Kær Helge, Sara Petrat-Melin, and Matilda Ejgreen Tjelldén. The Playing with the Sun logo was designed by Henrik Viking Hansen.

Valuable input and feedback was given by Celeste Moreno, Liam Nilsen, Andrew Sliwinski, Sarah Trahan, Sebastian Martin, Ryan Jenkins, and Mike Petrich.

## Supporting Institutions

Gitlab provides hosting for the project's technical repositories as well as the resources site as part of its program to support open source software. 