# Bill of Materials

## Introduction 

This is the Bill of Materials to make one Playing with the Sun Construction Kit. One "Kit" is enough for 1-3 learners to work with at a time (we usually have pairs work together in workshops). It consists of a solarengine, powerpack, motor, 1/2 of a crank charger (because two groups can share a single crank charger), 3 x .5 watt solar panels, 4 XT30 patch cables, 20 snap rivets, and about 1 meter length of 12mm plumber strap cut to smaller pieces.

Below is a description of the parts required to build each component.

## Circuit Boards

The circuit boards consist of a solarengine, powerpack, and motor. Boards can be fabricated at JLC PCB, Aisler, or any other electronics fabricator using the BOM and CAD files located in the [circuit boards repository](https://gitlab.com/playing-with-the-sun/pwts-const-kit-hardware). This isn't as simple as we'd like it to be yet, so you'll need someone familiar with electronics to place the order. If you need help, you can reach out.


## Crank Chargers 
In workshops, crank chargers can be shared, so you need only one charger for every two kits.

| Qty      | Description | Notes     |
| ---:        |    ----   |          --- |
| 1     | [Wind Hydraulic Generator Hand Crank Dynamo](https://www.aliexpress.com/i/1005004006385796.html)       | Also available on eBay. Be sure to buy one with crank handle.   |
| 1   | [XT30 pigail, female head, 30 cm long](https://www.aliexpress.com/item/1005003922672586.html)      | Be sure to select correct "color" and "package" options when ordering       |
| 2   | [Zener Diodes 5.1V 5W](https://www.mouser.dk/ProductDetail/onsemi/1N5338BG?qs=y2kkmE52mdMKTkCzLxg8vA%3D%3D&countryCode=DK&currencyCode=DKK)      | Limits voltage output of generator      |
| 1   | [Bridge Rectifier, 4A](https://www.mouser.dk/ProductDetail/Micro-Commercial-Components-MCC/UD4KB100-BP?qs=mzRxyRlhVdunQFn05Cy%2FVw%3D%3D&countryCode=DK&currencyCode=DKK)      | Makes crank work both ways      |
| 4   | [Barrier Terminal Block 1 POS](https://www.mouser.dk/ProductDetail/TE-Connectivity/1776293-1?qs=bdENzIhz2rkvnjQKk371fQ%3D%3D)      | These or any screw down wire connector should work to build the circuit in the charger  |
| 4   | [Clear Silicon tubing 36mm ID x 40mm OD](https://www.aliexpress.com/item/1005001640728440.html)      | This is used to protect the circuitry. Hose clamps are optional to hold it against the body of the generator. |


## Solar Panels
This design is "stackable" so the learner can use from 1 to 3 at a time. Quantities are for 3. The lasercut frames use 3mm plywood.

| Qty      | Description | Notes     |
| ---:        |    :----   |          --- |
| 3     | [0.5W Solar Panel 55x70](https://www.mouser.dk/ProductDetail/Seeed-Studio/313070004?qs=SElPoaY2y5IR7rJqVJryrQ%3D%3D)       | 70x55mm, 5 Volts   |
| 3     | [XT30PW-M Connector](https://www.tme.eu/en/details/xt30pw-m/dc-power-connectors/amass/)       | I wish Mouser had these, but they don't.   |
| 3     | [XT30PW-F Connector](https://www.tme.eu/en/details/xt30pw-f/dc-power-connectors/amass/)       |    |

## Structural Elements and Connectors

| Qty      | Description | Notes     |
| ---:        |    ----   |          --- |
| 20     | [SNAP RIVET T-TYPE .161"DIA .24"](https://www.digikey.dk/en/products/detail/essentra-components/SRT-4060B/394864?so=80231671&content=productdetail_DK)       | These are the "Dooter" we use to connect pieces of "hulbånd" or plumber's strap.|
| .1     | [Steel Plastic Coated Fixing Band, 10m x 12mm](https://dk.rs-online.com/web/p/patentband/0291048)       | This is made by BAND-IT, who says that AE465 stainless steel should be the same as AE565, galvanized steel. Quantity is .1 because you need one meter per kit, but minimum size is a 10m reel|
| 4     | [XT30 Patch cables, male to female, 10 cm](https://www.aliexpress.com/item/1005005563131079.html)       | Used for connecting electrical components. Be sure to choose correct options when ordering, i.e. "Color: male to female"|



