# The Wild Examples

The Teknologiforståelse Team at Aarhus Libraries made this eBook showcasing surprising and wild examples of creations that have emerged from various Playing with the Sun workshops. 

English Version (See below for Danish version)
<iframe src="https://read.bookcreator.com/GtVIwGb9ffxJ5oI64mPES0Ql2Fak6CAv5jJT-FeCA2I/RW1Ce10dSlG0RcwMfURxRg?sidebyside=false" width="700" height="700"></iframe>



Danish Version:
<iframe src="https://read.bookcreator.com/cgzJO60PSYhIf_9cAeyG3qjVl4xm_u5jctBL_EaY_g8/lXw2qc-QQqmgP7RP0EVcew?sidebyside=false" width="700" height="700"></iframe>

