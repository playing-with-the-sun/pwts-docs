# Pattern Machines

Pattern Machines make marks with ink or sand, and make a great introduction to Tinkering. Here's a brief introduction, in Danish, to the drawing machines activity using handcrank power. 

<div style="position: relative; padding-bottom: 56.25%; height: 0;">
<iframe aria-label="Playing with the Sun - Drawing Machines" type="text/html" style="position: absolute; top: 0; left: 0; width: 100%!important; height: 100%!important;" src=https://media.videotool.dk/?vn=467_2023121013412679513252100990 frameborder="0" allowfullscreen allow="autoplay; fullscreen"></iframe></div>

</br>
The books below (first English, then Danish) describe everything you need to know to prepare and run a pattern machines activity, as well as tips on how to facilitate it. 

<iframe src="https://read.bookcreator.com/AzV8VkW_bJ-zeISQlUPKrE6CmTRRfRGEnYDNuT-DYgI/KXMHDVOPT-2sZz4xQohFNQ/pdN8K8OJTzO7LyPdHiEsZw?sidebyside=false" width="700" height="700"></iframe>


Danish Version:
<iframe src="https://read.bookcreator.com/GtVIwGb9ffxJ5oI64mPES0Ql2Fak6CAv5jJT-FeCA2I/3jdG8Ju8RLK_hg08wUdA9g?sidebyside=false" width="700" height="700"></iframe>

