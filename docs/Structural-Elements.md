# Structural Elements

![Plumber's Strap and Pop Rivets](img/plumberStrap-popRivets.jpg)

These structural elements are both inexpensive and very flexible. The snap rivets fit inside the holes of the strap and lock, making it possible to quickly and firmly connect two pieces. One rivet makes a connection that pivots, and two makes one that is rigid. The rivet can be easily removed just by pulling, and most of the time it repositions its sliding collar so that it can be immediately placed in another strap hole. 

Up to three pieces of strap can be joined with one rivet. The hole diameters and distances match hose of the electronic components so it's easy to attach them to strap structures. 

Provide 5 or 6 pieces of 5 and 9 hole strap each to builders, as well as a generous handful of snap rivets.

## Sourcing Parts

1. Plumber's Strap (Danish Patentbånd) This is 12 cm PPA coated steel strap of the sort that Plumber's use to hang pipes with. The plastic coating helps to prevent sharp edges and feels better to the hands. It also helps the Snap Rivets seat nicely. You can source this from [RS Online](https://uk.rs-online.com/web/p/fixing-straps/0291048), and probably many others judging from web searches. Make sure that you get the same hole diameter / distance, as the circuit boards are keyed to fit these dimensions exactly.

2. Snap Rivets, T-TYPE .161" DIA .24" You can order them from [Digi-Key](https://www.digikey.dk/en/products/detail/essentra-components/SRT-4060B/394864?so=80231671&content=productdetail_DK) but they are not as inexpensive as they seem like they ought to be, considering that they are little plastic bits.  

## Assembly

Tools:

* Large metal shears, or cutters capable of snipping the strap.
* Metal file for dressing the edges of cuts

We like odd size lengths because they make it possible to make designs (especially creatures) with a center spine and equal lengths of strap to either side. 5 and 9 holes seem versatile for most purposes. But when we want to attach something else to the system - like leaves, or decorations, we often cut 2 hole lengths, with one hole to attach to the decoration and the other to attach (via snap rivet) to the rest of the project's structure. 

1. Using the shears, cut the desired length of strap, with the cut going through the solid portion between two holes. Shape the ends with the shears to make it somewhat rounded, so that there are no sharp corners. 

2. If the ends feel sharp, quickly file them down.
 

## Desired Improvements

* Sometimes people pull the sliding colar off of the rivet, which renders it inoperable. It would be better if it were one piece that couldn't be separated. 
* Is there an alternative that is less expensive, but still provides a firm grip? 