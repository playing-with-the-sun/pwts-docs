# Playing with the Sun Resources 

Designed for children and families in non-formal or informal learning environments, [Playing with the Sun](https://www.playingwiththesun.org/) invites learners to follow their curiosity and build something powered by solar, wind, or muscles. In the process they develop their understanding of sustainable energy sources and their ability to create with them. 

This is a repository of documentation and how tos for the [Playing with the Sun](https://www.playingwiththesun.org/) Project. Source code for all digitally fabricated components can be found in our [Gitlab Repository](https://gitlab.com/playing-with-the-sun). All of the information here is made available under a [Creative Commons Non-Commercial License](https://creativecommons.org/licenses/by-nc/4.0/). 

![inquiry](img/inquiry-scene.png)


## What is the PwtS Construction Kit?

The Playing with the Sun [construction kit](Const-Kit-Overview.md) is an open-ended building system that consists of solar panels, solarengines, motors, powerpacks, and hand-crank generators, all electronically and physically compatible with eachother. The system runs on 5V of power or less, with no batteries or constant current supply. 

With these components, parents and educators can offer a variety of creative learning [activities](Activities-Overview.md). These include drawing machines, cable crawlers, solar-bugs, and more, each of which invite children and adults to build something based on their own interests and curiosity.

If you want to build the kit yourself, you can find out where to get the parts you need and how to assemble them here. Playing with the Sun is an open source project funded by grants. We welcome contributions in the form of designs, activities, and funding. See the [Contributor Guide](Contributor-Guide.md) for more information. 
