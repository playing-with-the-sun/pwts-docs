# Power Packs

![PowerPack v3](img/power-packr3.jpg)

The power pack holds 5F of power in a ultracapacitor, which is like a battery but one that can fill up or empty out almost instantaneously. You can charge it  by plugging a power source like a solar panel or a hand crank generator into its input. Then it will provide power to run a motor for a minute or so - first quickly, then more slowly as it runs down.

These have to be ordered from an electronics manufacturer that can fabricate circuit boards. Once fabricated, no assembly is required.

## Desired Improvements

* Mark and Amos have thought about making one with two outputs for two motors that can "flip-flop" which output gets the power.

## Past Revisions

* The second revision had a small trim pot because we weren't sure what value of resistor to put on the output. These would get trimmed the wrong way sometimes, and it would seem like the motor was really slow for some reason, leading to confusion. So in rev 3 we removed the trim pot and went for a fixed 12 Ohm resistor.

* The first revision was smaller, made by hand on perf board.
![PowerPack v1](img/powerpack-v1.jpg)