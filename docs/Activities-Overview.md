# Overview of Playing with the Sun Activities

Playing with the Sun activities are Tinkering activities, which means they invite participants to learn through creative exploration and playful experimentation. These books were created by the Technological Literacy Team at Aarhus Public Libraries, with whom these activities were created. 

English Version (See below for Danish version)
<iframe src="https://read.bookcreator.com/AzV8VkW_bJ-zeISQlUPKrE6CmTRRfRGEnYDNuT-DYgI/plM_xlUaQmGX7x5NMfssdA/pdN8K8OJTzO7LyPdHiEsZw?sidebyside=false&nopreview" width="700" height="700"></iframe>


Danish Version:
<iframe src="https://read.bookcreator.com/GtVIwGb9ffxJ5oI64mPES0Ql2Fak6CAv5jJT-FeCA2I/Z4L3a5whTzONS248HzV_rQ/pdN8K8OJTzO7LyPdHiEsZw?sidebyside=false&nopreview" width="700" height="700"></iframe>