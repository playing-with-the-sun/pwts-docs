# Solar Engines

![SolarEngigine](img/solarengine-v3.jpg)

The Miller Solarengine stores up power until there is enough to make a pulse to the output. Plug a solarpanel into the input and a motor on the output. If you hold your hand over part of the solar panel, you'll see the motor start pulsing - faster when there is more sun, slower when there is less because it takes longer to store enough energy for a pulse. Revision 3 starts its pulse when its capacitor reaches 3.3 Volts, and stops (to start storing energy for the next pulse) when it reaches 3.0 Volts.

The [Miller Solarengine](http://solarbotics.net/library/circuits/se_t1_mse.html) was presumably invented by someone called "Miller," a BEAM robotics enthusiast. We learned about it from the great people at [Solarbotics](https://www.solarbotics.com/?s=miller+solarengine&post_type=product&dgwt_wcas=1).

There's no assembly. This one just needs to get fabricated by someone / some company capable of making and populating circuit boards.

## Desired Improvements

* It should be clearly distinguishable from the Powerpack, but unless you know what to look for, they look more or less the same. 
* Mark made one that had a tunable potentiometer on it that would vary the sensitivity / threshold. Super cool, but it's not yet clear how to explain what's going on to the user.
* Should these be integrated with the solar panels? That'd make a more versatile solar panel, and simplify things for the user. But maybe it's simplifying things too much?

## Past Revisions

* The first revision we used was the [Solderless SolarEngine](https://www.solarbotics.com/product/solderless-solarengine/) made by Solarbotics. We found that the 2.7 Volt trigger voltage was better for the blue motors we work with, as anything less makes for a rather weak pulse that can't turn a very large wheel.   