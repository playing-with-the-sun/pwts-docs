# Overview of the PwtS Construction Kit

The Playing with the Sun construction kit is an open-ended building system that consists of solar panels, solarengines, motors, powerpacks, and hand-crank generators, all of which are designed to be electronically and physically compatible with eachother. The system runs on 5V of power or less, with no batteries or constant current supply.

![Constructed Critter](img/critter-in-front-soft-back.jpg)

## How it Works in Play
[Solar panels](Solar-Panel.md) generate power in full sun -- they don't work well inside or on cloudy days. You can use a cable to connect one to a [motor](Motor.md), and you'll see the motor turn. But as soon as it hits a patch of shade or a cloud comes over, it will stop. Some creative electronics engineers in the 1980s invented a simple circuit called a "solarengine." A [solarengine](Solar-Engine.md) stores up power until there's enough to briefly pulse the motor. So putting one of these between your solar panel and your motor will make it keep going in a pulsing, staccato way, even when there's shady spots.

What if there's no sun, like most of the time in Denmark where this kit was developed? As with all sources of sustainable energy, when one source isn't available it's important to have an alternative. In this case, you can use a [hand-crank generator](Hand-Crank-Generator.md) to directly drive a motor. Or you can plug it into a [powerpack](Power-Pack.md) and crank it for about 20 seconds until the light on the powerpack shines bright white. Then you can plug the power pack into a [motor](Motor.md) and it will run for a minute or so until it needs charging again.

Once you understand how the different electronic elements work, you can begin to build projects with them using the various [structural elements](Structural-Elements.md). This system is based on inexpensive and bendable "Plumber's strap" (Danish: Patentbånd) with holes that allow pieces to be joined together firmly using snap rivets, which can then be easily removed to try something else.


## Introductory Video (Danish)

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe aria-label="Intro til Playing with the Sun kittet" type="text/html" style="position: absolute; top: 0; left: 0; width: 100%!important; height: 100%!important;" src=https://media.videotool.dk/?vn=467_2023121013330519220092451262&frameborder="0" allowfullscreen allow="autoplay; fullscreen"></iframe></div>

</br>

## Sourcing Parts and Building the Construction Kit
You cannot buy the PwtS construction kit. But you can build it yourself. This requires knowing how to use digital fabrication tools like laser cutters and 3D printers, as well as the ability to do basic soldering, working with epoxy, etc. And it requires sourcing a variety of parts and materials from various distributors. It's a great project for educators to build in partnership with a Makerspace, or for individuals to build with friends who are experienced hackers. All files necessary for digital fabrication are available in the project's [Gitlab repository](https://gitlab.com/playing-with-the-sun/).

## Limitations

Currently, the trickiest part of this build fabricating the electronic circuit boards for the solarengines, powerpacks, and motors. We haven't found an electronics fabricator that will help us set things up so you can "one-click" purchase these components yet. Without this they will be difficult to get made, even though all the source files are available in our [Gitlab repository](https://gitlab.com/playing-with-the-sun/). Once we figure out an easier solution for this, we'll update these docs.



