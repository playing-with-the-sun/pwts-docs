#Cable Crawlers

Cable Crawlers is an activity that invites learners to build solar and hand-crank powered machines that move across paracord strings. Below is a brief video, in Danish, introducing the activity.

<div style="position: relative; padding-bottom: 56.25%; height: 0;"><iframe aria-label="Playing with the Sun - Cable Crawlers" type="text/html" style="position: absolute; top: 0; left: 0; width: 100%!important; height: 100%!important;" src=https://media.videotool.dk/?vn=467_2023121013391757703079095680 frameborder="0" allowfullscreen allow="autoplay; fullscreen"></iframe></div>

</br>
The books below (first English, then Danish) describe everything you need to know to prepare and run the cable crawler activity, as well as tips on how to facilitate it. 

<iframe src="https://read.bookcreator.com/Wpnsy5p5WeXvlVDgJ-cWaYfSrVfpPrTzYfPmuV62J6k/QnxCxaudS6CJbBs7Kn9FRA/8aeBuCSKRaWsYhu6aNaTKw?sidebyside=false" width="700" height="700"></iframe>

Danish Version:
<iframe src="https://read.bookcreator.com/cgzJO60PSYhIf_9cAeyG3qjVl4xm_u5jctBL_EaY_g8/j4PIrTtwToqMmoaxoGTD-g?sidebyside=false" width="700" height="700"></iframe>
