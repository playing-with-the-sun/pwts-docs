# Contributor Guide

Playing with the Sun (PwtS) is a project that involves play materials, pedagogy, and community driven inquiry. The construction kit is open source, which means that anyone may contribute ideas for new activities and components. Doing that well requires a good understanding of the pedagogical and design goals behind the project. 

<img src="../img/Pwts_Drawing_collective-brain_v2.png" style="display: block; margin-left: auto; margin-right: auto; width: 50%;" >

The PwtS construction kit is designed with the principles of constructionist learning theory, sometimes called Tinkering or Creative learning, in mind. (See references in *Further Reading* below). The goal is to create the conditions for children's playful and creative experimentation so that they can explore what's interesting, relevant, and meaningful to them. This is distinct from many if not most educational technologies that are popular today, which are designed to deliver abstract concepts into children's minds regardless of what they care about or are interested in.

As the drivers of this design process, our goal is to ensure that all elements - the physical construction kit, activities, and associated pedagogy - are as clear, concise and understandable as possible for the learners and educators using them. If you have an idea that you think would be useful for others, try it out, document it, and reach out to us so we can discuss adding it in to the kit in order to make it available to others.

## Design Principles

### Low floor, High ceiling

A ‘Low floor ’ means that it is easy for the learner to orient themselves and get started with the playful activity (Resnick, 2017). The quintessential low-floor high ceiling play material is the Lego brick: It takes only a moment to understand how it works, and once you have that understanding you can build everything from a simple duck to a model of the Eiffel Tower. Components and interfaces - digital or otherwise - should be designed with a similar aspiration: they must be easy to use and understand, so that a wide range of learners with different competences can easily get started (low floor). But it should also be possible to build and explore complex ideas (high ceiling).

A tinkering activity or play material with a “low floor and high ceiling” will be interesting to a wide range of people in terms of age and experience, because each of them can engage with it at their own level.

*What it looks like:*

When a learner engages with something that’s confusing or overly complex – for example, a computer interface that’s difficult to immediately understand - they will show it. It often looks like reluctance to engage, with perhaps a few ineffectual stabs at trying something, followed by frowns, followed by a retreat to more familiar ground. Designers must watch for encounters with their activities and tools that look like this, and try to understand which aspects of the interface led to confusion and need to be simplified or clarified. 

### Make the relationship between energy generation and energy use tangible

A child is not allowed to drive trucks and construction machinery. Nor can she turn into a giant, walk up to a 60 meter wind turbine, and spin it to see what happens. Toys offer a way to explore these things outside of the constraints of practical reality. Children can play with the solar panels, motors, and generators in the PwtS construction kit. We want that play to enable them to build an intuitive, experience-based foundation for their understanding of sustainable energy. 

Most children today don't have a sense of how much energy comes out of a solar panel in the sun, or a wind turbine in the wind (and the same goes for most adults). Most toys run on batteries, which are 'black boxes' that make the entire energy creation and storage process abstract and experientially inaccessible. The child only understands that someone somewhere did something that resulted in this thing being able to power my toy, and at some point it will run out and I'll need a new one. 

The transition to sustainability will require that people understand how energy is created from sustainable sources, and how that must and should influence energy use. So PwtS activities often invite children to experiment with the production side of the energy use equation. What happens when I put my hand over this solar panel? What happens when I crank slowly vs. when I crank fast? This kind of play is about exploring the relationships between power sources and power loads, the understanding of which will be important to whatever project the child is building with the kit.

A related goal is to enable the child to build a sense of proportionality. She can see that enough energy falls from the sky in the form of sunlight to power her small, spinning machine. As soon as she tries replacing the solar panel with a powerpack she has charged up by cranking a generator, she will begin to develop a sense of their equivalency. Cranking the generator with her arm becomes a way to literally "get a feel for" the energy in the sunlight striking the solar panel.

*What it looks like:*

When a child plays with a drawing machine by shading it with their hand, or shining more light on the solar panel with a mirror, they are exploring this relationship. 

<video width="320" height="240" loop autoplay controls style="display: block; margin-left: auto; margin-right: auto; width: 50%;">
  <source src="../img/lander-mirror.mp4" type="video/mp4">
  Your browser does not support the video tag.
</video> 

 
 
### Make the construction kit as inexpensive, easy to source, and easy to build as possible

We want this construction kit to be easily sourced and built, so lots of people can use it and contribute to it. Given the choice between a high-tech expensive solar panel and one that is cheap and easy to get, the latter wins every time. The same goes for all the other elements. 


## Further Reading / References:

Blanton, A. (2019). [Some Principles of Open-Ended Learning through Play Activities](https://www.amosamos.net/index.html#principles-open-ended-thought). Amos Blanton’s Website: [https://www.amosamos.net/index.html#principles-open-ended-thought](https://www.amosamos.net/index.html#principles-open-ended-thought)

Resnick, M. (2017). [Lifelong kindergarten cultivating creativity through projects, passion, peers, and play.](https://mitpress.mit.edu/9780262536134/lifelong-kindergarten/) MIT Press. https://doi.org/10.7551/mitpress/11017.001.0001

Resnick, M., & Rosenbaum, E. (2013). [Designing for Tinkerability](https://www.media.mit.edu/publications/designing-for-tinkerability/). In M. Honey & D. Kanter (Eds.), Design, make, play: Growing the next generation of STEM innovators. Routledge.





