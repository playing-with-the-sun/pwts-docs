# Contact

If you have questions about Playing with the Sun, you can ask them in our [Playing with the Sun Matrix Room](https://app.element.io/#/room/#playingwiththesun:matrix.org), or you can write to 'pwts' and then the "at" symbol used for email followed by 'amosamos.net'  


<a href="https://app.element.io/#/room/#playingwiththesun:matrix.org">![matrix room](img/Pwts-Matrix-Room.png)</a> 