Playing with the Sun Resources website
Made with MkDocs
Hosted on Gitlab Pages (Thanks!)

---
[MkDocs](https://www.mkdocs.org/) is an open source static site generator that takes Markdown files and spits out static html. 

The PwtS Resources site is made from the collection of *.md files found in the /docs directory. To edit the site or make a change, you need to fork the project (make your own copy on gitlab). Then make any changes you want to. Then make a "pull request" - which will send your proposed changes to the maintainer for review. 

Improvements to the documentation are welcome! 
- Please be as succinct and clear as possible.
- Remember that this project is aimed at educators and not-so-technical folks. Avoid jargon when possible.
- Images are often worth 1000 words, as long as they are good images. 

---

## Building locally and Making Changes.

To work locally with this project, you'll have to follow the steps below:

1. Fork, clone or download this project
1. [Install](https://www.mkdocs.org/getting-started/) MkDocs
1. Preview your project: `mkdocs serve`,
   your site can be accessed under `localhost:8000`
1. Add content
1. Generate the website: `mkdocs build` (optional)
1. Make a pull request against this project, so your changes can be integrated into docs.playingwiththesun.org


Read more at MkDocs [documentation](https://www.mkdocs.org/user-guide/).



---

Forked from https://gitlab.com/morph027/mkdocs
